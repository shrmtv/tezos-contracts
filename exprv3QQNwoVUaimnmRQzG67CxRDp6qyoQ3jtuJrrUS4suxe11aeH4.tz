{ parameter
    (or (or (or (address %addCollaborator) (or (string %createCollection) (unit %default)))
            (or (address %delCollaborator)
                (or (pair %mint (string %ipfsmeta) (address %recipient)) (mutez %payRecipients))))
        (or (or (pair %send (mutez %amount) (address %receiver))
                (or (address %setAdmin) (nat %setCollection)))
            (or (mutez %setCost) (or (bool %togglePause) (string %updateCollection))))) ;
  storage
    (pair (pair (pair (address %admin) (nat %collection))
                (pair (mutez %cost) (big_map %metadata string bytes)))
          (pair (pair (int %minted) (address %objktFactoryAddress))
                (pair (bool %paused) (map %recipients address nat)))) ;
  code { LAMBDA
           (pair unit
                 (pair (pair (pair address nat) (pair mutez (big_map string bytes)))
                       (pair (pair int address) (pair bool (map address nat)))))
           (pair unit
                 (pair (pair (pair address nat) (pair mutez (big_map string bytes)))
                       (pair (pair int address) (pair bool (map address nat)))))
           { CDR ;
             DUP ;
             CAR ;
             CAR ;
             CAR ;
             SENDER ;
             COMPARE ;
             EQ ;
             IF {} { PUSH string "Only admin can call this entrypoint" ; FAILWITH } ;
             UNIT ;
             PAIR } ;
         SWAP ;
         LAMBDA
           (pair unit
                 (pair (pair (pair address nat) (pair mutez (big_map string bytes)))
                       (pair (pair int address) (pair bool (map address nat)))))
           (pair unit
                 (pair (pair (pair address nat) (pair mutez (big_map string bytes)))
                       (pair (pair int address) (pair bool (map address nat)))))
           { CDR ;
             DUP ;
             CAR ;
             GET 3 ;
             AMOUNT ;
             COMPARE ;
             GE ;
             IF {} { PUSH string "Amount too low" ; FAILWITH } ;
             UNIT ;
             PAIR } ;
         SWAP ;
         LAMBDA
           (pair unit
                 (pair (pair (pair address nat) (pair mutez (big_map string bytes)))
                       (pair (pair int address) (pair bool (map address nat)))))
           (pair unit
                 (pair (pair (pair address nat) (pair mutez (big_map string bytes)))
                       (pair (pair int address) (pair bool (map address nat)))))
           { CDR ;
             DUP ;
             CAR ;
             CAR ;
             CDR ;
             PUSH nat 0 ;
             COMPARE ;
             NEQ ;
             IF {} { PUSH string "Collection id not set" ; FAILWITH } ;
             UNIT ;
             PAIR } ;
         SWAP ;
         LAMBDA
           (pair unit
                 (pair (pair (pair address nat) (pair mutez (big_map string bytes)))
                       (pair (pair int address) (pair bool (map address nat)))))
           (pair unit
                 (pair (pair (pair address nat) (pair mutez (big_map string bytes)))
                       (pair (pair int address) (pair bool (map address nat)))))
           { CDR ;
             DUP ;
             GET 5 ;
             PUSH bool True ;
             COMPARE ;
             NEQ ;
             IF {} { PUSH string "Minter halted" ; FAILWITH } ;
             UNIT ;
             PAIR } ;
         SWAP ;
         { { DUP ; CAR ; DIP { CDR } } } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DIG 2 ;
                     DROP ;
                     DIG 3 ;
                     DROP ;
                     DIG 3 ;
                     UNIT ;
                     SWAP ;
                     DIG 3 ;
                     DIG 2 ;
                     PAIR ;
                     EXEC ;
                     CDR ;
                     DIG 2 ;
                     SWAP ;
                     DIG 2 ;
                     DIG 2 ;
                     UNIT ;
                     SWAP ;
                     DIG 3 ;
                     DIG 2 ;
                     PAIR ;
                     EXEC ;
                     CDR ;
                     SWAP ;
                     NIL operation ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     GET 3 ;
                     CDR ;
                     CONTRACT %invite_collaborator (pair address nat) ;
                     IF_NONE { PUSH string "No such entrypoint" ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     { DIP 4 { DUP } ; DIG 5 } ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DIG 4 ;
                     PAIR ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { IF_LEFT
                       { DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         SWAP ;
                         NIL operation ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 3 ;
                         CDR ;
                         CONTRACT %create_artist_collection bytes ;
                         IF_NONE { PUSH string "No such entrypoint" ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         PACK ;
                         PUSH nat 6 ;
                         DIG 5 ;
                         PACK ;
                         SIZE ;
                         SUB ;
                         ISNAT ;
                         IF_NONE { PUSH int 39 ; FAILWITH } {} ;
                         PUSH nat 6 ;
                         SLICE ;
                         IF_NONE { PUSH string "Could not get bytes of string" ; FAILWITH } {} ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { DROP ;
                         SWAP ;
                         DROP ;
                         SWAP ;
                         DROP ;
                         SWAP ;
                         DROP ;
                         SWAP ;
                         DROP ;
                         NIL operation } } }
               { IF_LEFT
                   { DIG 2 ;
                     DROP ;
                     DIG 3 ;
                     DROP ;
                     DIG 3 ;
                     UNIT ;
                     SWAP ;
                     DIG 3 ;
                     DIG 2 ;
                     PAIR ;
                     EXEC ;
                     CDR ;
                     DIG 2 ;
                     SWAP ;
                     DIG 2 ;
                     DIG 2 ;
                     UNIT ;
                     SWAP ;
                     DIG 3 ;
                     DIG 2 ;
                     PAIR ;
                     EXEC ;
                     CDR ;
                     SWAP ;
                     NIL operation ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     GET 3 ;
                     CDR ;
                     CONTRACT %remove_collaborator (pair address nat) ;
                     IF_NONE { PUSH string "No such entrypoint" ; FAILWITH } {} ;
                     PUSH mutez 0 ;
                     { DIP 4 { DUP } ; DIG 5 } ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DIG 4 ;
                     PAIR ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { IF_LEFT
                       { DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         DUG 4 ;
                         DUG 4 ;
                         DIG 3 ;
                         DIG 4 ;
                         DIG 3 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         DIG 3 ;
                         DIG 3 ;
                         DIG 2 ;
                         DIG 3 ;
                         DIG 3 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         DIG 2 ;
                         SWAP ;
                         DIG 2 ;
                         DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         { { DUP ; CAR ; DIP { CDR } } } ;
                         SWAP ;
                         { { DUP ; CAR ; DIP { CDR } } } ;
                         { { DUP ; CAR ; DIP { CDR } } } ;
                         PUSH int 1 ;
                         ADD ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         NIL operation ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 3 ;
                         CDR ;
                         CONTRACT %mint_artist (pair (pair nat nat) (pair bytes address)) ;
                         IF_NONE { PUSH string "No such entrypoint" ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         DUP ;
                         CDR ;
                         SWAP ;
                         DUP ;
                         DUG 5 ;
                         CAR ;
                         PACK ;
                         PUSH nat 6 ;
                         DIG 6 ;
                         CAR ;
                         PACK ;
                         SIZE ;
                         SUB ;
                         ISNAT ;
                         IF_NONE { PUSH int 39 ; FAILWITH } {} ;
                         PUSH nat 6 ;
                         SLICE ;
                         IF_NONE { PUSH string "Could not get bytes of string" ; FAILWITH } {} ;
                         PAIR ;
                         PUSH nat 1 ;
                         { DIP 5 { DUP } ; DIG 6 } ;
                         CAR ;
                         CAR ;
                         CDR ;
                         PAIR ;
                         PAIR ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { PUSH bool False ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 6 ;
                         SENDER ;
                         MEM ;
                         COMPARE ;
                         EQ ;
                         IF { PUSH string "Only recipients can call this entrypoint" ; FAILWITH } {} ;
                         NIL operation ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 6 ;
                         ITER { DUP ;
                                DUG 2 ;
                                CAR ;
                                CONTRACT unit ;
                                IF_NONE { PUSH int 184 ; FAILWITH } {} ;
                                PUSH nat 100 ;
                                DIG 3 ;
                                CDR ;
                                { DIP 4 { DUP } ; DIG 5 } ;
                                MUL ;
                                EDIV ;
                                IF_NONE { PUSH int 184 ; FAILWITH } {} ;
                                CAR ;
                                UNIT ;
                                TRANSFER_TOKENS ;
                                CONS } ;
                         SWAP ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP } } } }
           { IF_LEFT
               { IF_LEFT
                   { DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     UNIT ;
                     SWAP ;
                     DIG 3 ;
                     DIG 2 ;
                     PAIR ;
                     EXEC ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     CDR ;
                     CONTRACT unit ;
                     IF_NONE { PUSH int 78 ; FAILWITH } {} ;
                     NIL operation ;
                     SWAP ;
                     DIG 2 ;
                     CAR ;
                     UNIT ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { IF_LEFT
                       { DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         { { DUP ; CAR ; DIP { CDR } } } ;
                         { { DUP ; CAR ; DIP { CDR } } } ;
                         CDR ;
                         DIG 3 ;
                         PAIR ;
                         PAIR ;
                         PAIR }
                       { DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         { { DUP ; CAR ; DIP { CDR } } } ;
                         { { DUP ; CAR ; DIP { CDR } } } ;
                         CAR ;
                         DIG 3 ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         PAIR } ;
                     NIL operation } }
               { IF_LEFT
                   { DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     DROP ;
                     DIG 2 ;
                     UNIT ;
                     SWAP ;
                     DIG 3 ;
                     DIG 2 ;
                     PAIR ;
                     EXEC ;
                     CDR ;
                     { { DUP ; CAR ; DIP { CDR } } } ;
                     { { DUP ; CAR ; DIP { CDR } } } ;
                     SWAP ;
                     CDR ;
                     DIG 3 ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     NIL operation }
                   { IF_LEFT
                       { DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         SWAP ;
                         UPDATE 5 ;
                         NIL operation }
                       { DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         DROP ;
                         DIG 2 ;
                         UNIT ;
                         SWAP ;
                         DIG 3 ;
                         DIG 2 ;
                         PAIR ;
                         EXEC ;
                         CDR ;
                         SWAP ;
                         NIL operation ;
                         { DIP 2 { DUP } ; DIG 3 } ;
                         GET 3 ;
                         CDR ;
                         CONTRACT %update_artist_collection (pair nat bytes) ;
                         IF_NONE { PUSH string "No such entrypoint" ; FAILWITH } {} ;
                         PUSH mutez 0 ;
                         { DIP 3 { DUP } ; DIG 4 } ;
                         PACK ;
                         PUSH nat 6 ;
                         DIG 5 ;
                         PACK ;
                         SIZE ;
                         SUB ;
                         ISNAT ;
                         IF_NONE { PUSH int 39 ; FAILWITH } {} ;
                         PUSH nat 6 ;
                         SLICE ;
                         IF_NONE { PUSH string "Could not get bytes of string" ; FAILWITH } {} ;
                         { DIP 4 { DUP } ; DIG 5 } ;
                         CAR ;
                         CAR ;
                         CDR ;
                         PAIR ;
                         TRANSFER_TOKENS ;
                         CONS } } } } ;
         NIL operation ;
         SWAP ;
         ITER { CONS } ;
         PAIR } }
