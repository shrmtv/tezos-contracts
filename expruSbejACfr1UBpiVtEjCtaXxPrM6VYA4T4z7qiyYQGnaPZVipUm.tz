{ parameter
    (or (or (or (unit %executeTimelock)
                (pair %propose
                   (nat %escrowAmount)
                   (pair %proposal
                      (string %title)
                      (pair (string %descriptionLink)
                            (pair (string %descriptionHash) (lambda %proposalLambda unit (list operation)))))))
            (or (pair %rescueFA12
                   (address %tokenContractAddress)
                   (pair (nat %amount) (address %destination)))
                (or (pair %rescueFA2
                       (address %tokenContractAddress)
                       (pair (nat %tokenId) (pair (nat %amount) (address %destination))))
                    (address %rescueXTZ))))
        (or (or (address %rotateOwner) (address %setDaoContractAddress))
            (or (address %setGovernorContract) (or (nat %vote) (nat %withdraw))))) ;
  storage
    (pair (pair (pair (nat %amountPerBlock) (nat %amountWithdrawn))
                (pair (address %daoContractAddress) (address %governorAddress)))
          (pair (pair (big_map %metadata string bytes) (address %owner))
                (pair (nat %startBlock) (address %tokenContractAddress)))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DROP ;
                     DUP ;
                     CDR ;
                     CAR ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_OWNER" ; FAILWITH } ;
                     DUP ;
                     CAR ;
                     CDR ;
                     CAR ;
                     CONTRACT %executeTimelock unit ;
                     { IF_NONE { PUSH int 297 ; FAILWITH } {} } ;
                     NIL operation ;
                     SWAP ;
                     PUSH mutez 0 ;
                     UNIT ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_OWNER" ; FAILWITH } ;
                     NIL operation ;
                     DUP 3 ;
                     GET 6 ;
                     CONTRACT %approve (pair (address %spender) (nat %value)) ;
                     { IF_NONE { PUSH int 245 ; FAILWITH } {} } ;
                     PUSH mutez 0 ;
                     PUSH nat 0 ;
                     DUP 6 ;
                     CAR ;
                     CDR ;
                     CAR ;
                     PAIR %spender %value ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     DUP 3 ;
                     GET 6 ;
                     CONTRACT %approve (pair (address %spender) (nat %value)) ;
                     { IF_NONE { PUSH int 245 ; FAILWITH } {} } ;
                     PUSH mutez 0 ;
                     DUP 4 ;
                     CAR ;
                     DUP 6 ;
                     CAR ;
                     CDR ;
                     CAR ;
                     PAIR %spender %value ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     DUP 3 ;
                     CAR ;
                     CDR ;
                     CAR ;
                     CONTRACT %propose
                       (pair (string %title)
                             (pair (string %descriptionLink)
                                   (pair (string %descriptionHash) (lambda %proposalLambda unit (list operation))))) ;
                     { IF_NONE { PUSH int 264 ; FAILWITH } {} } ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     CDR ;
                     TRANSFER_TOKENS ;
                     CONS } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CDR ;
                     CAR ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_OWNER" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 6 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     COMPARE ;
                     NEQ ;
                     IF {} { PUSH string "USE_WITHDRAW_INSTEAD" ; FAILWITH } ;
                     DUP ;
                     CAR ;
                     CONTRACT %transfer (pair (address %from) (pair (address %to) (nat %value))) ;
                     { IF_NONE { PUSH int 125 ; FAILWITH } {} } ;
                     NIL operation ;
                     SWAP ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     DUP ;
                     GET 3 ;
                     SWAP ;
                     GET 4 ;
                     PAIR %to %value ;
                     SELF_ADDRESS ;
                     PAIR %from ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         CAR ;
                         CDR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_OWNER" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 6 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         COMPARE ;
                         NEQ ;
                         IF {} { PUSH string "USE_WITHDRAW_INSTEAD" ; FAILWITH } ;
                         DUP ;
                         CAR ;
                         CONTRACT %transfer
                           (list (pair (address %from_)
                                       (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                         { IF_NONE { PUSH int 155 ; FAILWITH } {} } ;
                         NIL operation ;
                         SWAP ;
                         PUSH mutez 0 ;
                         NIL (pair (address %from_)
                                   (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))) ;
                         NIL (pair (address %to_) (pair (nat %token_id) (nat %amount))) ;
                         DIG 5 ;
                         DUP ;
                         GET 5 ;
                         SWAP ;
                         DUP ;
                         DUG 7 ;
                         GET 3 ;
                         PAIR %token_id %amount ;
                         DIG 6 ;
                         GET 6 ;
                         PAIR %to_ ;
                         CONS ;
                         SELF_ADDRESS ;
                         PAIR %from_ %txs ;
                         CONS ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         CAR ;
                         CDR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_OWNER" ; FAILWITH } ;
                         CONTRACT unit ;
                         { IF_NONE { PUSH int 107 ; FAILWITH } {} } ;
                         NIL operation ;
                         SWAP ;
                         BALANCE ;
                         UNIT ;
                         TRANSFER_TOKENS ;
                         CONS } } } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_GOVERNOR" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_GOVERNOR" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     CDR ;
                     DIG 3 ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR } ;
                 NIL operation }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CDR ;
                     CDR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NOT_GOVERNOR" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     NIL operation }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         CAR ;
                         CDR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_OWNER" ; FAILWITH } ;
                         NIL operation ;
                         DUP 3 ;
                         CAR ;
                         CDR ;
                         CAR ;
                         CONTRACT %vote nat ;
                         { IF_NONE { PUSH int 281 ; FAILWITH } {} } ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CDR ;
                         CAR ;
                         CDR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "NOT_OWNER" ; FAILWITH } ;
                         SWAP ;
                         DUP ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SWAP ;
                         DUP ;
                         DUG 3 ;
                         GET 5 ;
                         LEVEL ;
                         SUB ;
                         ISNAT ;
                         { IF_NONE { PUSH int 73 ; FAILWITH } {} } ;
                         MUL ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         DUP 4 ;
                         CAR ;
                         CAR ;
                         CDR ;
                         ADD ;
                         COMPARE ;
                         LE ;
                         IF {} { PUSH string "NOT_VESTED" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         DUP 5 ;
                         ADD ;
                         SWAP ;
                         PAIR ;
                         PAIR ;
                         PAIR ;
                         SWAP ;
                         NIL operation ;
                         DUP 3 ;
                         GET 6 ;
                         CONTRACT %transfer (pair (address %from) (pair (address %to) (nat %value))) ;
                         { IF_NONE { PUSH int 82 ; FAILWITH } {} } ;
                         PUSH mutez 0 ;
                         DIG 3 ;
                         SENDER ;
                         PAIR %to %value ;
                         SELF_ADDRESS ;
                         PAIR %from ;
                         TRANSFER_TOKENS ;
                         CONS } } } } ;
         NIL operation ;
         SWAP ;
         ITER { CONS } ;
         PAIR } }
