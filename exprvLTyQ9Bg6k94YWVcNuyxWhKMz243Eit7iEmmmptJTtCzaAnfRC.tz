{ parameter
    (or (or %adminAndInteract
           (or (or (or %admin (or (unit %confirm_admin) (bool %pause)) (address %set_admin))
                   (nat %bid))
               (or (nat %cancel) (nat %resolve)))
           (never %update_allowed))
        (pair %configure
           (mutez %opening_price)
           (pair (nat %min_raise_percent)
                 (pair (mutez %min_raise)
                       (pair (nat %round_time)
                             (pair (nat %extend_time)
                                   (pair (list %asset
                                            (pair (address %fa2_address) (list %fa2_batch (pair (nat %token_id) (nat %amount)))))
                                         (pair (list %beneficiaries (pair (address %address) (nat %percentage)))
                                               (pair (timestamp %start_time) (timestamp %end_time)))))))))) ;
  storage
    (pair (option %admin
             (pair (pair (address %admin) (bool %paused)) (option %pending_admin address)))
          (pair (nat %current_id)
                (pair (nat %max_auction_time)
                      (pair (nat %max_config_to_start_time)
                            (pair (big_map %auctions
                                     nat
                                     (pair (address %seller)
                                           (pair (mutez %current_bid)
                                                 (pair (timestamp %start_time)
                                                       (pair (timestamp %last_bid_time)
                                                             (pair (int %round_time)
                                                                   (pair (int %extend_time)
                                                                         (pair (list %asset
                                                                                  (pair (address %fa2_address) (list %fa2_batch (pair (nat %token_id) (nat %amount)))))
                                                                               (pair (nat %min_raise_percent)
                                                                                     (pair (mutez %min_raise)
                                                                                           (pair (timestamp %end_time)
                                                                                                 (pair (address %highest_bidder)
                                                                                                       (list %beneficiaries (pair (address %address) (nat %percentage)))))))))))))))
                                  (unit %allowlist)))))) ;
  code { LAMBDA
           (option (pair (pair address bool) (option address)))
           unit
           { IF_NONE
               { UNIT }
               { CAR ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 NEQ ;
                 IF { PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UNIT } } } ;
         LAMBDA
           (option (pair (pair address bool) (option address)))
           unit
           { IF_NONE
               { UNIT }
               { CAR ; CDR ; IF { PUSH string "PAUSED" ; FAILWITH } { UNIT } } } ;
         LAMBDA
           (pair bool string)
           unit
           { UNPAIR ; NOT ; IF { FAILWITH } { DROP ; UNIT } } ;
         DUP ;
         LAMBDA
           (pair (lambda (pair bool string) unit) string)
           unit
           { UNPAIR ;
             SWAP ;
             PUSH string "DONT_TRANSFER_TEZ_TO_" ;
             CONCAT ;
             PUSH mutez 0 ;
             AMOUNT ;
             COMPARE ;
             EQ ;
             PAIR ;
             EXEC } ;
         SWAP ;
         APPLY ;
         LAMBDA
           (pair mutez address)
           operation
           { UNPAIR ;
             SWAP ;
             CONTRACT unit ;
             IF_NONE { PUSH string "ADDRESS_DOES_NOT_RESOLVE" ; FAILWITH } {} ;
             SWAP ;
             UNIT ;
             TRANSFER_TOKENS } ;
         LAMBDA
           (pair (pair (list (pair address (list (pair nat nat)))) address) address)
           (list operation)
           { UNPAIR ;
             UNPAIR ;
             MAP { DUP ;
                   CDR ;
                   MAP { DUP 4 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         DIG 2 ;
                         CDR ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR } ;
                   SWAP ;
                   CAR ;
                   CONTRACT %transfer
                     (list (pair (address %from_)
                                 (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))) ;
                   IF_NONE { PUSH string "ADDRESS_DOES_NOT_RESOLVE" ; FAILWITH } {} ;
                   PUSH mutez 0 ;
                   NIL (pair address (list (pair address (pair nat nat)))) ;
                   DUP 5 ;
                   DIG 4 ;
                   SWAP ;
                   PAIR ;
                   CONS ;
                   TRANSFER_TOKENS } ;
             SWAP ;
             DIG 2 ;
             DROP 2 } ;
         LAMBDA
           (pair nat
                 (pair (option (pair (pair address bool) (option address)))
                       (pair nat
                             (pair nat
                                   (pair nat
                                         (pair (big_map
                                                  nat
                                                  (pair address
                                                        (pair mutez
                                                              (pair timestamp
                                                                    (pair timestamp
                                                                          (pair int
                                                                                (pair int
                                                                                      (pair (list (pair address (list (pair nat nat))))
                                                                                            (pair nat (pair mutez (pair timestamp (pair address (list (pair address nat))))))))))))))
                                               unit))))))
           (pair address
                 (pair mutez
                       (pair timestamp
                             (pair timestamp
                                   (pair int
                                         (pair int
                                               (pair (list (pair address (list (pair nat nat))))
                                                     (pair nat (pair mutez (pair timestamp (pair address (list (pair address nat)))))))))))))
           { UNPAIR ;
             SWAP ;
             GET 9 ;
             SWAP ;
             GET ;
             IF_NONE { PUSH string "AUCTION_DOES_NOT_EXIST" ; FAILWITH } {} } ;
         LAMBDA
           (pair address
                 (pair mutez
                       (pair timestamp
                             (pair timestamp
                                   (pair int
                                         (pair int
                                               (pair (list (pair address (list (pair nat nat))))
                                                     (pair nat (pair mutez (pair timestamp (pair address (list (pair address nat)))))))))))))
           bool
           { DUP ;
             GET 9 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             GET 7 ;
             ADD ;
             NOW ;
             COMPARE ;
             GT ;
             SWAP ;
             GET 19 ;
             NOW ;
             COMPARE ;
             GE ;
             AND } ;
         LAMBDA
           (pair address
                 (pair mutez
                       (pair timestamp
                             (pair timestamp
                                   (pair int
                                         (pair int
                                               (pair (list (pair address (list (pair nat nat))))
                                                     (pair nat (pair mutez (pair timestamp (pair address (list (pair address nat)))))))))))))
           bool
           { DUP ; CAR ; SWAP ; GET 21 ; COMPARE ; EQ } ;
         DUP ;
         LAMBDA
           (pair (lambda
                    (pair address
                          (pair mutez
                                (pair timestamp
                                      (pair timestamp
                                            (pair int
                                                  (pair int
                                                        (pair (list (pair address (list (pair nat nat))))
                                                              (pair nat (pair mutez (pair timestamp (pair address (list (pair address nat)))))))))))))
                    bool)
                 (pair address
                       (pair mutez
                             (pair timestamp
                                   (pair timestamp
                                         (pair int
                                               (pair int
                                                     (pair (list (pair address (list (pair nat nat))))
                                                           (pair nat (pair mutez (pair timestamp (pair address (list (pair address nat))))))))))))))
           bool
           { UNPAIR ; SWAP ; EXEC } ;
         SWAP ;
         APPLY ;
         DUP 6 ;
         LAMBDA
           (pair (lambda (pair mutez address) operation)
                 (pair (pair (list operation) (pair nat mutez)) (pair address nat)))
           (pair (list operation) (pair nat mutez))
           { UNPAIR ;
             SWAP ;
             UNPAIR ;
             UNPAIR ;
             SWAP ;
             UNPAIR ;
             PUSH nat 10000 ;
             DUP 5 ;
             CDR ;
             DUP 4 ;
             MUL ;
             EDIV ;
             IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
             CAR ;
             DUP 5 ;
             CAR ;
             SWAP ;
             PAIR ;
             DIG 5 ;
             SWAP ;
             EXEC ;
             DIG 4 ;
             CDR ;
             DIG 2 ;
             ADD ;
             DIG 2 ;
             SWAP ;
             PAIR ;
             DUG 2 ;
             CONS ;
             PAIR } ;
         SWAP ;
         APPLY ;
         DIG 11 ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DIG 2 ;
                     DIG 7 ;
                     DIG 9 ;
                     DROP 3 ;
                     IF_LEFT
                       { DIG 2 ;
                         DIG 3 ;
                         DIG 4 ;
                         DIG 5 ;
                         DIG 6 ;
                         DIG 7 ;
                         DIG 8 ;
                         DROP 7 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         SWAP ;
                         IF_LEFT
                           { IF_LEFT
                               { DIG 3 ;
                                 DROP 2 ;
                                 IF_NONE
                                   { PUSH string "NO_ADMIN_CAPABILITIES_CONFIGURED" ; FAILWITH }
                                   { DUP ;
                                     CDR ;
                                     IF_NONE
                                       { DROP ; PUSH string "NO_PENDING_ADMIN" ; FAILWITH }
                                       { SENDER ;
                                         COMPARE ;
                                         EQ ;
                                         IF { NONE address ; SWAP ; CAR ; CDR ; SENDER ; PAIR ; PAIR ; SOME }
                                            { DROP ; PUSH string "NOT_A_PENDING_ADMIN" ; FAILWITH } } } ;
                                 NIL operation ;
                                 PAIR }
                               { SWAP ;
                                 DUP ;
                                 DUG 2 ;
                                 DIG 4 ;
                                 SWAP ;
                                 EXEC ;
                                 DROP ;
                                 SWAP ;
                                 IF_NONE
                                   { DROP ; PUSH string "NO_ADMIN_CAPABILITIES_CONFIGURED" ; FAILWITH }
                                   { DUP ; CDR ; DUG 2 ; CAR ; CAR ; PAIR ; PAIR ; SOME } ;
                                 NIL operation ;
                                 PAIR } }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             DIG 4 ;
                             SWAP ;
                             EXEC ;
                             DROP ;
                             SWAP ;
                             IF_NONE
                               { DROP ; PUSH string "NO_ADMIN_CAPABILITIES_CONFIGURED" ; FAILWITH }
                               { SWAP ; SOME ; SWAP ; CAR ; PAIR ; SOME } ;
                             NIL operation ;
                             PAIR } ;
                         UNPAIR ;
                         DUG 2 ;
                         UPDATE 1 ;
                         SWAP ;
                         PAIR }
                       { DIG 9 ;
                         DROP ;
                         AMOUNT ;
                         SENDER ;
                         DUP 4 ;
                         DUP 4 ;
                         PAIR ;
                         DIG 8 ;
                         SWAP ;
                         EXEC ;
                         DIG 4 ;
                         DIG 2 ;
                         DIG 3 ;
                         PAIR ;
                         DIG 2 ;
                         DIG 3 ;
                         DIG 2 ;
                         UNPAIR ;
                         PUSH string "CALLER_NOT_IMPLICIT" ;
                         SOURCE ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         PAIR ;
                         DUP 11 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         DUP 5 ;
                         CAR ;
                         DIG 11 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         PUSH string "NOT_IN_PROGRESS" ;
                         DUP 5 ;
                         DIG 9 ;
                         SWAP ;
                         EXEC ;
                         NOT ;
                         DUP 6 ;
                         GET 5 ;
                         NOW ;
                         COMPARE ;
                         GE ;
                         AND ;
                         PAIR ;
                         DUP 10 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         PUSH string "SEllER_CANT_BID" ;
                         DUP 5 ;
                         CAR ;
                         DUP 4 ;
                         COMPARE ;
                         NEQ ;
                         PAIR ;
                         DUP 10 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         PUSH string "NO_SELF_OUTBIDS" ;
                         DUP 5 ;
                         GET 21 ;
                         DUP 4 ;
                         COMPARE ;
                         NEQ ;
                         PAIR ;
                         DIG 9 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         DUP ;
                         DUP 5 ;
                         DUP ;
                         DIG 9 ;
                         SWAP ;
                         EXEC ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 3 ;
                         DUP 4 ;
                         COMPARE ;
                         GE ;
                         AND ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         GET 17 ;
                         DUP 3 ;
                         GET 3 ;
                         ADD ;
                         DUP 4 ;
                         COMPARE ;
                         GE ;
                         DUP 3 ;
                         GET 3 ;
                         DUP 4 ;
                         GET 15 ;
                         PUSH nat 100 ;
                         SWAP ;
                         DIG 2 ;
                         MUL ;
                         EDIV ;
                         IF_NONE
                           { PUSH string "DIVISION_BY_ZERO" ; FAILWITH }
                           { UNPAIR ;
                             PUSH mutez 0 ;
                             DIG 2 ;
                             COMPARE ;
                             GT ;
                             IF { PUSH mutez 1 ; ADD } {} } ;
                         DIG 3 ;
                         GET 3 ;
                         ADD ;
                         DIG 3 ;
                         COMPARE ;
                         GE ;
                         OR ;
                         OR ;
                         NOT ;
                         IF { NOW ;
                              DUP 5 ;
                              GET 7 ;
                              DUP 6 ;
                              GET 21 ;
                              PAIR ;
                              AMOUNT ;
                              DUP 7 ;
                              GET 3 ;
                              PAIR ;
                              PAIR ;
                              PAIR ;
                              PUSH string "INVALID_BID_AMOUNT" ;
                              PAIR ;
                              FAILWITH }
                            {} ;
                         DUP 4 ;
                         DIG 6 ;
                         SWAP ;
                         EXEC ;
                         IF { DIG 5 ; DROP ; NIL operation }
                            { DUP 4 ;
                              GET 21 ;
                              DUP 5 ;
                              GET 3 ;
                              PAIR ;
                              DIG 6 ;
                              SWAP ;
                              EXEC ;
                              NIL operation ;
                              SWAP ;
                              CONS } ;
                         DUP 5 ;
                         GET 11 ;
                         NOW ;
                         DUP 7 ;
                         GET 19 ;
                         SUB ;
                         COMPARE ;
                         LE ;
                         IF { DUP 5 ; GET 11 ; NOW ; ADD } { DUP 5 ; GET 19 } ;
                         DIG 5 ;
                         DIG 3 ;
                         UPDATE 3 ;
                         DIG 3 ;
                         UPDATE 21 ;
                         NOW ;
                         UPDATE 7 ;
                         SWAP ;
                         UPDATE 19 ;
                         DIG 3 ;
                         DUP ;
                         GET 9 ;
                         DIG 2 ;
                         SOME ;
                         DIG 4 ;
                         UPDATE ;
                         UPDATE 9 ;
                         SWAP ;
                         PAIR } }
                   { DIG 4 ;
                     DIG 12 ;
                     DROP 2 ;
                     IF_LEFT
                       { DIG 2 ;
                         DROP ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         DIG 10 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         PAIR ;
                         DIG 5 ;
                         SWAP ;
                         EXEC ;
                         DUP ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF { UNIT }
                            { PUSH string "OR_A_SELLER" ;
                              DUP 4 ;
                              CAR ;
                              IF_NONE
                                { DROP ; UNIT }
                                { CAR ;
                                  CAR ;
                                  SENDER ;
                                  COMPARE ;
                                  NEQ ;
                                  IF { PUSH string "_" ; CONCAT ; PUSH string "NOT_AN_ADMIN" ; CONCAT ; FAILWITH }
                                     { DROP ; UNIT } } } ;
                         DROP ;
                         PUSH string "AUCTION_ENDED" ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         DIG 6 ;
                         SWAP ;
                         EXEC ;
                         NOT ;
                         PAIR ;
                         DIG 8 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         PUSH string "CANCEL" ;
                         DIG 7 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         DUP ;
                         CAR ;
                         SELF_ADDRESS ;
                         DUP 3 ;
                         GET 13 ;
                         PAIR ;
                         PAIR ;
                         DIG 5 ;
                         SWAP ;
                         EXEC ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         DIG 5 ;
                         SWAP ;
                         EXEC ;
                         IF { SWAP ; DIG 4 ; DROP 2 }
                            { SWAP ;
                              DUP ;
                              DUG 2 ;
                              GET 21 ;
                              DIG 2 ;
                              GET 3 ;
                              PAIR ;
                              DIG 4 ;
                              SWAP ;
                              EXEC ;
                              CONS } ;
                         DIG 2 ;
                         DUP ;
                         GET 9 ;
                         DIG 3 ;
                         NONE (pair address
                                    (pair mutez
                                          (pair timestamp
                                                (pair timestamp
                                                      (pair int
                                                            (pair int
                                                                  (pair (list (pair address (list (pair nat nat))))
                                                                        (pair nat (pair mutez (pair timestamp (pair address (list (pair address nat))))))))))))) ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 9 ;
                         SWAP ;
                         PAIR }
                       { DIG 7 ;
                         DROP ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         DIG 10 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         PAIR ;
                         DIG 6 ;
                         SWAP ;
                         EXEC ;
                         PUSH string "AUCTION_NOT_ENDED" ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         DIG 7 ;
                         SWAP ;
                         EXEC ;
                         PAIR ;
                         DIG 8 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         PUSH string "RESOLVE" ;
                         DIG 7 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         DUP ;
                         GET 21 ;
                         SELF_ADDRESS ;
                         DUP 3 ;
                         GET 13 ;
                         PAIR ;
                         PAIR ;
                         DIG 6 ;
                         SWAP ;
                         EXEC ;
                         SWAP ;
                         DUP ;
                         DUG 2 ;
                         DIG 6 ;
                         SWAP ;
                         EXEC ;
                         IF { SWAP ; DIG 4 ; DROP 2 }
                            { SWAP ;
                              DUP ;
                              DUG 2 ;
                              GET 3 ;
                              PUSH nat 0 ;
                              PAIR ;
                              NIL operation ;
                              PAIR ;
                              DIG 2 ;
                              GET 22 ;
                              ITER { SWAP ; PAIR ; DUP 5 ; SWAP ; EXEC } ;
                              DIG 4 ;
                              DROP ;
                              UNPAIR ;
                              SWAP ;
                              CAR ;
                              PUSH nat 10000 ;
                              SWAP ;
                              COMPARE ;
                              EQ ;
                              IF { ITER { CONS } }
                                 { DROP 2 ; PUSH string "INVALID_BENEFICIARIES" ; FAILWITH } } ;
                         DIG 2 ;
                         DUP ;
                         GET 9 ;
                         DIG 3 ;
                         NONE (pair address
                                    (pair mutez
                                          (pair timestamp
                                                (pair timestamp
                                                      (pair int
                                                            (pair int
                                                                  (pair (list (pair address (list (pair nat nat))))
                                                                        (pair nat (pair mutez (pair timestamp (pair address (list (pair address nat))))))))))))) ;
                         SWAP ;
                         UPDATE ;
                         UPDATE 9 ;
                         SWAP ;
                         PAIR } } }
               { DIG 2 ;
                 DIG 3 ;
                 DIG 4 ;
                 DIG 5 ;
                 DIG 6 ;
                 DIG 7 ;
                 DIG 8 ;
                 DIG 9 ;
                 DIG 10 ;
                 DIG 11 ;
                 DIG 12 ;
                 DROP 11 ;
                 SWAP ;
                 DROP ;
                 NEVER } }
           { DIG 3 ;
             DIG 4 ;
             DIG 5 ;
             DIG 6 ;
             DIG 8 ;
             DIG 12 ;
             DROP 6 ;
             SWAP ;
             SENDER ;
             DUP 3 ;
             DUP 3 ;
             CAR ;
             DIG 9 ;
             SWAP ;
             EXEC ;
             DROP ;
             DUP 3 ;
             GET 10 ;
             SWAP ;
             DUP ;
             DUG 2 ;
             GET 11 ;
             ITER { PUSH string "ASSET_NOT_ALLOWED" ; DUP 3 ; DROP 3 } ;
             DROP ;
             PUSH string "INVALID_END_TIME" ;
             SWAP ;
             DUP ;
             DUG 2 ;
             GET 15 ;
             DUP 3 ;
             GET 16 ;
             COMPARE ;
             GT ;
             PAIR ;
             DUP 9 ;
             SWAP ;
             EXEC ;
             DROP ;
             PUSH string "INVALID_AUCTION_TIME" ;
             DUP 4 ;
             GET 5 ;
             DUP 3 ;
             GET 15 ;
             DUP 4 ;
             GET 16 ;
             SUB ;
             ABS ;
             COMPARE ;
             LE ;
             PAIR ;
             DUP 9 ;
             SWAP ;
             EXEC ;
             DROP ;
             PUSH string "INVALID_START_TIME" ;
             NOW ;
             DUP 3 ;
             GET 15 ;
             COMPARE ;
             GE ;
             PAIR ;
             DUP 9 ;
             SWAP ;
             EXEC ;
             DROP ;
             PUSH string "MAX_CONFIG_TO_START_TIME_VIOLATED" ;
             DUP 4 ;
             GET 7 ;
             NOW ;
             DUP 4 ;
             GET 15 ;
             SUB ;
             ABS ;
             COMPARE ;
             LE ;
             PAIR ;
             DUP 9 ;
             SWAP ;
             EXEC ;
             DROP ;
             PUSH string "INVALID_OPENING_PRICE" ;
             PUSH mutez 0 ;
             DUP 3 ;
             CAR ;
             COMPARE ;
             GT ;
             PAIR ;
             DUP 9 ;
             SWAP ;
             EXEC ;
             DROP ;
             PUSH string "CONFIGURE" ;
             DIG 7 ;
             SWAP ;
             EXEC ;
             DROP ;
             PUSH string "INVALID_ROUND_TIME" ;
             PUSH nat 0 ;
             DUP 3 ;
             GET 7 ;
             COMPARE ;
             GT ;
             PAIR ;
             DUP 8 ;
             SWAP ;
             EXEC ;
             DROP ;
             PUSH string "INVALID_RAISE_CONFIGURATION" ;
             PUSH mutez 0 ;
             DUP 3 ;
             GET 5 ;
             COMPARE ;
             GT ;
             PUSH nat 0 ;
             DUP 4 ;
             GET 3 ;
             COMPARE ;
             GT ;
             AND ;
             PAIR ;
             DUP 8 ;
             SWAP ;
             EXEC ;
             DROP ;
             PUSH mutez 0 ;
             PUSH nat 0 ;
             PAIR ;
             NIL operation ;
             PAIR ;
             SWAP ;
             DUP ;
             DUG 2 ;
             GET 13 ;
             ITER { SWAP ; PAIR ; DUP 6 ; SWAP ; EXEC } ;
             DIG 5 ;
             DROP ;
             CDR ;
             CAR ;
             PUSH string "INVALID_BENEFICIARIES" ;
             PUSH nat 10000 ;
             DIG 2 ;
             COMPARE ;
             EQ ;
             PAIR ;
             DIG 6 ;
             SWAP ;
             EXEC ;
             DROP ;
             DIG 2 ;
             DUP ;
             DUP ;
             DUG 4 ;
             GET 9 ;
             DUP 4 ;
             DUP 4 ;
             CAR ;
             DUP 5 ;
             GET 15 ;
             DUP 6 ;
             GET 15 ;
             DUP 7 ;
             GET 7 ;
             INT ;
             DUP 8 ;
             GET 9 ;
             INT ;
             DUP 9 ;
             GET 11 ;
             DUP 10 ;
             GET 3 ;
             DUP 11 ;
             GET 5 ;
             DUP 12 ;
             GET 16 ;
             DIG 13 ;
             DIG 13 ;
             GET 13 ;
             SWAP ;
             PAIR ;
             SWAP ;
             PAIR ;
             SWAP ;
             PAIR ;
             SWAP ;
             PAIR ;
             SWAP ;
             PAIR ;
             SWAP ;
             PAIR ;
             SWAP ;
             PAIR ;
             SWAP ;
             PAIR ;
             SWAP ;
             PAIR ;
             SWAP ;
             PAIR ;
             SWAP ;
             PAIR ;
             SOME ;
             DUP 4 ;
             GET 3 ;
             UPDATE ;
             UPDATE 9 ;
             PUSH nat 1 ;
             DIG 2 ;
             GET 3 ;
             ADD ;
             UPDATE 3 ;
             SELF_ADDRESS ;
             SENDER ;
             DIG 3 ;
             GET 11 ;
             PAIR ;
             PAIR ;
             DIG 2 ;
             SWAP ;
             EXEC ;
             PAIR } } }
