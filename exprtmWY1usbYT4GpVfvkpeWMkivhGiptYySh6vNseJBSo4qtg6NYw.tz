{ parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (pair %mint
                   (pair (address %address) (nat %amount))
                   (pair (map %metadata string bytes) (nat %token_id))))
            (or (address %set_administrator) (pair %set_metadata (string %k) (bytes %v))))
        (or (or (bool %set_pause)
                (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount)))))))
            (or (list %update_operators
                   (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                       (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id)))))
                (list %update_token_metadata (pair (nat %token_id) (map %token_info string bytes)))))) ;
  storage
    (pair (pair (address %administrator)
                (pair (big_map %ledger nat address) (big_map %metadata string bytes)))
          (pair (pair (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit)
                      (bool %paused))
                (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                      (big_map %total_supply nat nat)))) ;
  code { CAST (pair (or (or (or (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat))))
                                (pair (pair address nat) (pair (map string bytes) nat)))
                            (or address (pair string bytes)))
                        (or (or bool (list (pair address (list (pair address (pair nat nat))))))
                            (or (list (or (pair address (pair address nat)) (pair address (pair address nat))))
                                (list (pair nat (map string bytes))))))
                    (pair (pair address (pair (big_map nat address) (big_map string bytes)))
                          (pair (pair (big_map (pair address (pair address nat)) unit) bool)
                                (pair (big_map nat (pair nat (map string bytes))) (big_map nat nat))))) ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CDR ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     CAR ;
                     MAP { DUP 3 ;
                           GET 5 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           DUP 3 ;
                           CAR ;
                           GET 3 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF { DUP ;
                                CAR ;
                                DUP 4 ;
                                CAR ;
                                GET 3 ;
                                DUP 3 ;
                                CDR ;
                                GET ;
                                IF_NONE { PUSH int 429 ; FAILWITH } {} ;
                                COMPARE ;
                                EQ ;
                                IF { PUSH nat 1 ; SWAP ; PAIR } { PUSH nat 0 ; SWAP ; PAIR } }
                              { PUSH nat 0 ; SWAP ; PAIR } } ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     CAR ;
                     CDR ;
                     PUSH nat 1 ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "NFT-asset: amount <> 1" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 3 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 4 ;
                     MEM ;
                     IF { PUSH string "NFT-asset: cannot mint twice same token" ; FAILWITH } {} ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     DUP 5 ;
                     CAR ;
                     CAR ;
                     SOME ;
                     DUP 6 ;
                     GET 4 ;
                     UPDATE ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     DUP ;
                     DUG 2 ;
                     GET 5 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 4 ;
                     MEM ;
                     IF { SWAP ;
                          DUP ;
                          GET 6 ;
                          DUP 3 ;
                          CAR ;
                          CDR ;
                          SOME ;
                          DIG 3 ;
                          GET 4 ;
                          UPDATE ;
                          UPDATE 6 }
                        { SWAP ;
                          DUP ;
                          GET 5 ;
                          DIG 2 ;
                          DUP ;
                          GET 3 ;
                          SWAP ;
                          DUP ;
                          DUG 4 ;
                          GET 4 ;
                          PAIR ;
                          SOME ;
                          DUP 4 ;
                          GET 4 ;
                          UPDATE ;
                          UPDATE 5 ;
                          DUP ;
                          GET 6 ;
                          DUP 3 ;
                          CAR ;
                          CDR ;
                          SOME ;
                          DIG 3 ;
                          GET 4 ;
                          UPDATE ;
                          UPDATE 6 } ;
                     NIL operation } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     CDR ;
                     DIG 2 ;
                     PAIR ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     DUP 5 ;
                     CDR ;
                     SOME ;
                     DIG 5 ;
                     CAR ;
                     UPDATE ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CDR ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP ;
                            CDR ;
                            ITER { DUP 4 ;
                                   CAR ;
                                   CAR ;
                                   SENDER ;
                                   COMPARE ;
                                   EQ ;
                                   IF { PUSH bool True } { SENDER ; DUP 3 ; CAR ; COMPARE ; EQ } ;
                                   IF { PUSH bool True }
                                      { DUP 4 ;
                                        GET 3 ;
                                        CAR ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        GET 3 ;
                                        SENDER ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR 3 ;
                                        MEM } ;
                                   IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                   DUP 4 ;
                                   GET 5 ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   GET 3 ;
                                   MEM ;
                                   IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                   DUP ;
                                   GET 4 ;
                                   PUSH nat 2 ;
                                   COMPARE ;
                                   GT ;
                                   IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                   DUP ;
                                   GET 4 ;
                                   PUSH nat 0 ;
                                   COMPARE ;
                                   LT ;
                                   IF { SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        CAR ;
                                        DUP 5 ;
                                        CAR ;
                                        GET 3 ;
                                        DUP 3 ;
                                        GET 3 ;
                                        GET ;
                                        IF_NONE { PUSH int 412 ; FAILWITH } {} ;
                                        COMPARE ;
                                        EQ ;
                                        IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                        DIG 3 ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        SWAP ;
                                        UNPAIR ;
                                        DUP 5 ;
                                        CAR ;
                                        SOME ;
                                        DIG 5 ;
                                        GET 3 ;
                                        UPDATE ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        DUG 2 }
                                      { DROP } } ;
                            DROP } ;
                     DROP } }
               { IF_LEFT
                   { DUP ;
                     ITER { IF_LEFT
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF { PUSH bool True } { DUP 3 ; CAR ; CAR ; SENDER ; COMPARE ; EQ } ;
                                IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                DIG 2 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                PUSH (option unit) (Some Unit) ;
                                DIG 5 ;
                                DUP ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                GET 3 ;
                                SWAP ;
                                CAR ;
                                PAIR 3 ;
                                UPDATE ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP }
                              { DUP ;
                                CAR ;
                                SENDER ;
                                COMPARE ;
                                EQ ;
                                IF { PUSH bool True } { DUP 3 ; CAR ; CAR ; SENDER ; COMPARE ; EQ } ;
                                IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                DIG 2 ;
                                UNPAIR ;
                                SWAP ;
                                UNPAIR ;
                                UNPAIR ;
                                NONE unit ;
                                DIG 5 ;
                                DUP ;
                                GET 4 ;
                                SWAP ;
                                DUP ;
                                GET 3 ;
                                SWAP ;
                                CAR ;
                                PAIR 3 ;
                                UPDATE ;
                                PAIR ;
                                PAIR ;
                                SWAP ;
                                PAIR ;
                                SWAP } } ;
                     DROP }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     DUP ;
                     ITER { DIG 2 ;
                            DUP ;
                            GET 5 ;
                            DIG 2 ;
                            DUP ;
                            SOME ;
                            SWAP ;
                            CAR ;
                            UPDATE ;
                            UPDATE 5 ;
                            SWAP } ;
                     DROP } } ;
             NIL operation } ;
         PAIR } }
