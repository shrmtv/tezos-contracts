{ parameter
    (or (or (or (pair %balance_of
                   (list %requests (pair (address %owner) (nat %token_id)))
                   (contract %callback
                      (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance)))))
                (pair %mint
                   (pair (address %address) (nat %issuer_id))
                   (pair (map %metadata string bytes) (nat %royalties))))
            (or (address %set_administrator)
                (or (address %set_issuer) (pair %set_metadata (string %k) (bytes %v)))))
        (or (or (bool %set_pause) (address %set_treasury_address))
            (or (list %transfer
                   (pair (address %from_)
                         (list %txs (pair (address %to_) (pair (nat %token_id) (nat %amount))))))
                (or (mutez %transfer_xtz_treasury)
                    (list %update_operators
                       (or (pair %add_operator (address %owner) (pair (address %operator) (nat %token_id)))
                           (pair %remove_operator (address %owner) (pair (address %operator) (nat %token_id))))))))) ;
  storage
    (pair (pair (pair (address %administrator) (nat %all_tokens))
                (pair (address %issuer)
                      (pair (big_map %ledger (pair address nat) nat) (big_map %metadata string bytes))))
          (pair (pair (big_map %operators
                         (pair (address %owner) (pair (address %operator) (nat %token_id)))
                         unit)
                      (bool %paused))
                (pair (big_map %token_data nat (pair (nat %issuer_id) (nat %royalties)))
                      (pair (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))
                            (address %treasury_address))))) ;
  code { CAST (pair (or (or (or (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat))))
                                (pair (pair address nat) (pair (map string bytes) nat)))
                            (or address (or address (pair string bytes))))
                        (or (or bool address)
                            (or (list (pair address (list (pair address (pair nat nat)))))
                                (or mutez (list (or (pair address (pair address nat)) (pair address (pair address nat))))))))
                    (pair (pair (pair address nat)
                                (pair address (pair (big_map (pair address nat) nat) (big_map string bytes))))
                          (pair (pair (big_map (pair address (pair address nat)) unit) bool)
                                (pair (big_map nat (pair nat nat))
                                      (pair (big_map nat (pair nat (map string bytes))) address))))) ;
         UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CDR ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     CAR ;
                     MAP { DUP 3 ;
                           GET 7 ;
                           SWAP ;
                           DUP ;
                           DUG 2 ;
                           CDR ;
                           MEM ;
                           IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                           DUP 3 ;
                           CAR ;
                           GET 5 ;
                           SWAP ;
                           DUP ;
                           CDR ;
                           SWAP ;
                           DUP ;
                           DUG 3 ;
                           CAR ;
                           PAIR ;
                           MEM ;
                           IF { DUP 3 ;
                                CAR ;
                                GET 5 ;
                                SWAP ;
                                DUP ;
                                CDR ;
                                SWAP ;
                                DUP ;
                                DUG 3 ;
                                CAR ;
                                PAIR ;
                                GET ;
                                IF_NONE { PUSH int 421 ; FAILWITH } {} ;
                                SWAP ;
                                PAIR }
                              { PUSH nat 0 ; SWAP ; PAIR } } ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 3 ;
                     TRANSFER_TOKENS ;
                     CONS }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     GET 3 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     PUSH (option nat) (Some 1) ;
                     DIG 7 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     DUP 8 ;
                     CAR ;
                     CAR ;
                     PAIR ;
                     UPDATE ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     DUP ;
                     DUG 2 ;
                     DUP ;
                     GET 7 ;
                     DUP 3 ;
                     GET 3 ;
                     DUP 5 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     PAIR ;
                     SOME ;
                     DIG 4 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     UPDATE ;
                     UPDATE 7 ;
                     DUP ;
                     DUG 2 ;
                     DUP ;
                     GET 5 ;
                     DIG 2 ;
                     DUP ;
                     GET 4 ;
                     SWAP ;
                     CAR ;
                     CDR ;
                     PAIR ;
                     SOME ;
                     DIG 3 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     UPDATE ;
                     UPDATE 5 ;
                     DUP ;
                     CAR ;
                     CAR ;
                     CDR ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "Token-IDs should be consecutive" ; FAILWITH } ;
                     DUP ;
                     UNPAIR ;
                     UNPAIR ;
                     UNPAIR ;
                     SWAP ;
                     PUSH nat 1 ;
                     DIG 5 ;
                     CAR ;
                     CAR ;
                     CDR ;
                     ADD ;
                     SWAP ;
                     DUP ;
                     DUP 3 ;
                     COMPARE ;
                     LE ;
                     IF { SWAP ; DROP } { DROP } ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     PAIR ;
                     NIL operation } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     UNPAIR ;
                     CDR ;
                     DIG 3 ;
                     PAIR ;
                     PAIR ;
                     PAIR }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         CDR ;
                         DIG 3 ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR }
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         SWAP ;
                         UNPAIR ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         UNPAIR ;
                         SWAP ;
                         DUP 6 ;
                         CDR ;
                         SOME ;
                         DIG 6 ;
                         CAR ;
                         UPDATE ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         SWAP ;
                         PAIR ;
                         PAIR } } ;
                 NIL operation } }
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     CAR ;
                     DIG 3 ;
                     SWAP ;
                     PAIR ;
                     PAIR ;
                     SWAP ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     CAR ;
                     CAR ;
                     CAR ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                     UPDATE 8 } ;
                 NIL operation }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     CDR ;
                     IF { PUSH string "FA2_PAUSED" ; FAILWITH } {} ;
                     DUP ;
                     ITER { DUP ;
                            CDR ;
                            ITER { DUP 4 ;
                                   CAR ;
                                   CAR ;
                                   CAR ;
                                   SENDER ;
                                   COMPARE ;
                                   EQ ;
                                   IF { PUSH bool True } { SENDER ; DUP 3 ; CAR ; COMPARE ; EQ } ;
                                   IF { PUSH bool True }
                                      { DUP 4 ;
                                        GET 3 ;
                                        CAR ;
                                        SWAP ;
                                        DUP ;
                                        DUG 2 ;
                                        GET 3 ;
                                        SENDER ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR 3 ;
                                        MEM } ;
                                   IF { PUSH bool True } { SELF_ADDRESS ; SENDER ; COMPARE ; EQ } ;
                                   IF {} { PUSH string "FA2_NOT_OPERATOR" ; FAILWITH } ;
                                   DUP 4 ;
                                   GET 7 ;
                                   SWAP ;
                                   DUP ;
                                   DUG 2 ;
                                   GET 3 ;
                                   MEM ;
                                   IF {} { PUSH string "FA2_TOKEN_UNDEFINED" ; FAILWITH } ;
                                   DUP ;
                                   GET 4 ;
                                   PUSH nat 0 ;
                                   COMPARE ;
                                   LT ;
                                   IF { DUP ;
                                        GET 4 ;
                                        DUP 5 ;
                                        CAR ;
                                        GET 5 ;
                                        DUP 3 ;
                                        GET 3 ;
                                        DUP 5 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH int 400 ; FAILWITH } {} ;
                                        COMPARE ;
                                        GE ;
                                        IF {} { PUSH string "FA2_INSUFFICIENT_BALANCE" ; FAILWITH } ;
                                        DUP 4 ;
                                        UNPAIR ;
                                        UNPAIR ;
                                        SWAP ;
                                        UNPAIR ;
                                        SWAP ;
                                        UNPAIR ;
                                        DUP ;
                                        DUP 7 ;
                                        GET 3 ;
                                        DUP 9 ;
                                        CAR ;
                                        PAIR ;
                                        DUP ;
                                        DUG 2 ;
                                        GET ;
                                        IF_NONE { PUSH int 404 ; FAILWITH } { DROP } ;
                                        DUP 7 ;
                                        GET 4 ;
                                        DIG 10 ;
                                        CAR ;
                                        GET 5 ;
                                        DUP 9 ;
                                        GET 3 ;
                                        DUP 11 ;
                                        CAR ;
                                        PAIR ;
                                        GET ;
                                        IF_NONE { PUSH int 404 ; FAILWITH } {} ;
                                        SUB ;
                                        ISNAT ;
                                        IF_NONE { PUSH int 404 ; FAILWITH } {} ;
                                        SOME ;
                                        SWAP ;
                                        UPDATE ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        SWAP ;
                                        PAIR ;
                                        PAIR ;
                                        DUP ;
                                        DUG 4 ;
                                        CAR ;
                                        GET 5 ;
                                        SWAP ;
                                        DUP ;
                                        GET 3 ;
                                        SWAP ;
                                        DUP ;
                                        DUG 3 ;
                                        CAR ;
                                        PAIR ;
                                        MEM ;
                                        IF { DIG 3 ;
                                             UNPAIR ;
                                             UNPAIR ;
                                             SWAP ;
                                             UNPAIR ;
                                             SWAP ;
                                             UNPAIR ;
                                             DUP ;
                                             DIG 6 ;
                                             DUP ;
                                             GET 3 ;
                                             SWAP ;
                                             DUP ;
                                             DUG 8 ;
                                             CAR ;
                                             PAIR ;
                                             DUP ;
                                             DUG 2 ;
                                             GET ;
                                             IF_NONE { PUSH int 406 ; FAILWITH } {} ;
                                             DIG 7 ;
                                             GET 4 ;
                                             ADD ;
                                             SOME ;
                                             SWAP ;
                                             UPDATE ;
                                             PAIR ;
                                             SWAP ;
                                             PAIR ;
                                             SWAP ;
                                             PAIR ;
                                             PAIR ;
                                             DUG 2 }
                                           { DIG 3 ;
                                             UNPAIR ;
                                             UNPAIR ;
                                             SWAP ;
                                             UNPAIR ;
                                             SWAP ;
                                             UNPAIR ;
                                             DUP 6 ;
                                             GET 4 ;
                                             SOME ;
                                             DIG 6 ;
                                             DUP ;
                                             GET 3 ;
                                             SWAP ;
                                             CAR ;
                                             PAIR ;
                                             UPDATE ;
                                             PAIR ;
                                             SWAP ;
                                             PAIR ;
                                             SWAP ;
                                             PAIR ;
                                             PAIR ;
                                             DUG 2 } }
                                      { DROP } } ;
                            DROP } ;
                     DROP ;
                     NIL operation }
                   { IF_LEFT
                       { SWAP ;
                         DUP ;
                         DUG 2 ;
                         CAR ;
                         CAR ;
                         CAR ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         IF {} { PUSH string "FA2_NOT_ADMIN" ; FAILWITH } ;
                         DUP ;
                         BALANCE ;
                         COMPARE ;
                         GE ;
                         IF {} { PUSH string "INSUFFISCIENT_BALANCE" ; FAILWITH } ;
                         NIL operation ;
                         DUP 3 ;
                         GET 8 ;
                         CONTRACT unit ;
                         IF_NONE { PUSH string "XTZ_TRANSFER_FAILED" ; FAILWITH } {} ;
                         DIG 2 ;
                         UNIT ;
                         TRANSFER_TOKENS ;
                         CONS }
                       { DUP ;
                         ITER { IF_LEFT
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF { PUSH bool True } { DUP 3 ; CAR ; CAR ; CAR ; SENDER ; COMPARE ; EQ } ;
                                    IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                    DIG 2 ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    PUSH (option unit) (Some Unit) ;
                                    DIG 5 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    PAIR ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP }
                                  { DUP ;
                                    CAR ;
                                    SENDER ;
                                    COMPARE ;
                                    EQ ;
                                    IF { PUSH bool True } { DUP 3 ; CAR ; CAR ; CAR ; SENDER ; COMPARE ; EQ } ;
                                    IF {} { PUSH string "FA2_NOT_ADMIN_OR_OPERATOR" ; FAILWITH } ;
                                    DIG 2 ;
                                    UNPAIR ;
                                    SWAP ;
                                    UNPAIR ;
                                    UNPAIR ;
                                    NONE unit ;
                                    DIG 5 ;
                                    DUP ;
                                    GET 4 ;
                                    SWAP ;
                                    DUP ;
                                    GET 3 ;
                                    SWAP ;
                                    CAR ;
                                    PAIR 3 ;
                                    UPDATE ;
                                    PAIR ;
                                    PAIR ;
                                    SWAP ;
                                    PAIR ;
                                    SWAP } } ;
                         DROP ;
                         NIL operation } } } } ;
         PAIR } }
