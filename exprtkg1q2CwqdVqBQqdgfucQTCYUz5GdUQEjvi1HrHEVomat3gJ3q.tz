{ view "containsArchId"
       nat
       bool
       { UNPAIR ;
         DIP { CDR ; CDR ; UNPAIR ; SWAP ; DROP } ;
         UNIT ;
         DUP 3 ;
         DUP 3 ;
         MEM ;
         SWAP ;
         DROP ;
         DIP { DROP 2 } } ;
  view "getMaxBalance"
       nat
       (option nat)
       { UNPAIR ;
         DIP { CDR ; CDR ; UNPAIR ; SWAP ; DROP } ;
         UNIT ;
         DUP 3 ;
         DUP 3 ;
         GET ;
         IF_NONE
           { PUSH string "archetypeLedger" ;
             PUSH string "AssetNotFound" ;
             PAIR ;
             FAILWITH }
           {} ;
         CDR ;
         CAR ;
         SWAP ;
         DROP ;
         DIP { DROP 2 } } ;
  view "getValidator"
       nat
       address
       { UNPAIR ;
         DIP { CDR ; CDR ; UNPAIR ; SWAP ; DROP } ;
         UNIT ;
         DUP 3 ;
         DUP 3 ;
         GET ;
         IF_NONE
           { PUSH string "archetypeLedger" ;
             PUSH string "AssetNotFound" ;
             PAIR ;
             FAILWITH }
           {} ;
         CAR ;
         SWAP ;
         DROP ;
         DIP { DROP 2 } } ;
  view "getRoyalties"
       nat
       (list (pair (address %partAccount) (nat %partValue)))
       { UNPAIR ;
         DIP { CDR ; CDR ; UNPAIR ; SWAP ; DROP } ;
         UNIT ;
         NIL (pair (address %partAccount) (nat %partValue)) ;
         DUP 4 ;
         DUP 4 ;
         MEM ;
         IF { DUP 4 ;
              DUP 4 ;
              GET ;
              IF_NONE
                { PUSH string "archetypeLedger" ;
                  PUSH string "AssetNotFound" ;
                  PAIR ;
                  FAILWITH }
                {} ;
              CDR ;
              CDR }
            { DUP } ;
         DIP { DIG 1 ; DROP } ;
         DUG 1 ;
         DROP ;
         DIP { DROP 2 } } ;
  parameter
    (or (or (or (bytes %setMetadataUri) (address %setAdminCandidate))
            (or (unit %acceptAdminCandidate) (address %setArchetype)))
        (or (or (pair %setValidator (nat %archid) (address %v))
                (pair %setMaxBalanceAllowed (nat %archid) (option %v nat)))
            (or (pair %setRoyalties
                   (nat %archid)
                   (list %v (pair (address %partAccount) (nat %partValue))))
                (pair %add
                   (nat %id)
                   (pair (address %mintingValidator) (option %imaxBalanceAllowed nat)))))) ;
  storage
    (pair (address %admin)
          (pair (option %archetype address)
                (pair (big_map %archetypeLedger
                         nat
                         (pair (address %archValidator)
                               (pair (option %maxBalanceAllowed nat)
                                     (list %royalties (pair (address %partAccount) (nat %partValue))))))
                      (pair (option %adminCandidate address) (big_map %metadata string bytes))))) ;
  code { UNPAIR ;
         DIP { UNPAIR 5 } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { DUP 2 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DIG 5 ;
                     DUP 2 ;
                     SOME ;
                     PUSH string "" ;
                     UPDATE ;
                     DUG 5 ;
                     DROP ;
                     PAIR 5 ;
                     NIL operation ;
                     PAIR }
                   { DUP 2 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DUP ;
                     SOME ;
                     DIP { DIG 4 ; DROP } ;
                     DUG 4 ;
                     DROP ;
                     PAIR 5 ;
                     NIL operation ;
                     PAIR } }
               { IF_LEFT
                   { DROP ;
                     DUP 4 ;
                     IF_NONE
                       { PUSH string "ADMIN_CANDIDATE_NOT_SET" ; FAILWITH }
                       { DUP ;
                         SENDER ;
                         COMPARE ;
                         EQ ;
                         NOT ;
                         IF { PUSH string "CALLER_NOT_ADMIN_CANDIDATE" ; FAILWITH } {} ;
                         DUP ;
                         DIP { DIG 1 ; DROP } ;
                         DUG 1 ;
                         NONE address ;
                         DIP { DIG 4 ; DROP } ;
                         DUG 4 ;
                         DROP } ;
                     PAIR 5 ;
                     NIL operation ;
                     PAIR }
                   { DUP 2 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DUP ;
                     SOME ;
                     DIP { DIG 2 ; DROP } ;
                     DUG 2 ;
                     DROP ;
                     PAIR 5 ;
                     NIL operation ;
                     PAIR } } }
           { IF_LEFT
               { IF_LEFT
                   { UNPAIR ;
                     SWAP ;
                     DUP 4 ;
                     IF_NONE { PUSH string "ARCHETYPE_NOT_SET" ; FAILWITH } {} ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     DUP 4 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     OR ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DUP 5 ;
                     DUP 6 ;
                     DUP 4 ;
                     GET ;
                     IF_NONE
                       { PUSH string "archetypeLedger" ;
                         PUSH string "AssetNotFound" ;
                         PAIR ;
                         FAILWITH }
                       {} ;
                     UNPAIR ;
                     DROP ;
                     DUP 3 ;
                     PAIR ;
                     SOME ;
                     DUP 4 ;
                     UPDATE ;
                     DIP { DIG 4 ; DROP } ;
                     DUG 4 ;
                     DROP 2 ;
                     PAIR 5 ;
                     NIL operation ;
                     PAIR }
                   { UNPAIR ;
                     SWAP ;
                     DUP 4 ;
                     IF_NONE { PUSH string "ARCHETYPE_NOT_SET" ; FAILWITH } {} ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     DUP 4 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     OR ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DUP 5 ;
                     DUP 6 ;
                     DUP 4 ;
                     GET ;
                     IF_NONE
                       { PUSH string "archetypeLedger" ;
                         PUSH string "AssetNotFound" ;
                         PAIR ;
                         FAILWITH }
                       {} ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     DROP ;
                     DUP 4 ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     SOME ;
                     DUP 4 ;
                     UPDATE ;
                     DIP { DIG 4 ; DROP } ;
                     DUG 4 ;
                     DROP 2 ;
                     PAIR 5 ;
                     NIL operation ;
                     PAIR } }
               { IF_LEFT
                   { UNPAIR ;
                     SWAP ;
                     DUP 4 ;
                     IF_NONE { PUSH string "ARCHETYPE_NOT_SET" ; FAILWITH } {} ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     DUP 4 ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     OR ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DUP 5 ;
                     DUP 6 ;
                     DUP 4 ;
                     GET ;
                     IF_NONE
                       { PUSH string "archetypeLedger" ;
                         PUSH string "AssetNotFound" ;
                         PAIR ;
                         FAILWITH }
                       {} ;
                     UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     DROP ;
                     DUP 4 ;
                     SWAP ;
                     PAIR ;
                     SWAP ;
                     PAIR ;
                     SOME ;
                     DUP 4 ;
                     UPDATE ;
                     DIP { DIG 4 ; DROP } ;
                     DUG 4 ;
                     DROP 2 ;
                     PAIR 5 ;
                     NIL operation ;
                     PAIR }
                   { UNPAIR ;
                     SWAP ;
                     UNPAIR ;
                     SWAP ;
                     DUP 5 ;
                     IF_NONE { PUSH string "NotFound" ; FAILWITH } {} ;
                     SENDER ;
                     COMPARE ;
                     EQ ;
                     NOT ;
                     IF { PUSH string "InvalidCaller" ; FAILWITH } {} ;
                     DUP 6 ;
                     DUP 4 ;
                     MEM ;
                     IF { PUSH string "ARCHETYPE_ALREADY_REGISTERED" ; FAILWITH } {} ;
                     DUP 6 ;
                     DUP 4 ;
                     MEM ;
                     IF { PUSH string "archetypeLedger" ; PUSH string "KeyExists" ; PAIR ; FAILWITH }
                        { DUP 6 ;
                          NIL (pair (address %partAccount) (nat %partValue)) ;
                          DUP 3 ;
                          PAIR ;
                          DUP 4 ;
                          PAIR ;
                          SOME ;
                          DUP 5 ;
                          UPDATE ;
                          DIP { DIG 5 ; DROP } ;
                          DUG 5 } ;
                     DROP 3 ;
                     PAIR 5 ;
                     NIL operation ;
                     PAIR } } } } }
