{ parameter
    (or (or (or (pair %approve (address %spender) (nat %value))
                (pair %getAllowance
                   (pair %request (address %owner) (address %spender))
                   (contract %callback nat)))
            (or (pair %getBalance (address %owner) (contract %callback nat))
                (pair %getTotalSupply (unit %request) (contract %callback nat))))
        (or (pair %mintOrBurn (int %quantity) (address %target))
            (pair %transfer (address %from) (pair (address %to) (nat %value))))) ;
  storage
    (pair (big_map %tokens address nat)
          (pair (big_map %allowances (pair (address %owner) (address %spender)) nat)
                (pair (address %admin)
                      (pair (nat %total_supply)
                            (pair (big_map %metadata string bytes)
                                  (big_map %token_metadata nat (pair (nat %token_id) (map %token_info string bytes)))))))) ;
  code { { { DUP ; CAR ; DIP { CDR } } } ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     GET 3 ;
                     SENDER ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CAR ;
                     SWAP ;
                     PAIR ;
                     PUSH nat 0 ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     CDR ;
                     COMPARE ;
                     GT ;
                     PUSH nat 0 ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     { DIP 3 { DUP } ; DIG 4 } ;
                     GET ;
                     IF_NONE { PUSH nat 0 } {} ;
                     COMPARE ;
                     GT ;
                     AND ;
                     IF { PUSH string "UnsafeAllowanceChange" ; FAILWITH } {} ;
                     DIG 3 ;
                     DIG 2 ;
                     DIG 3 ;
                     CDR ;
                     PUSH nat 0 ;
                     SWAP ;
                     DUP ;
                     DUG 2 ;
                     COMPARE ;
                     EQ ;
                     IF { DROP ; NONE nat } { SOME } ;
                     DIG 3 ;
                     UPDATE ;
                     UPDATE 3 ;
                     NIL operation ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     NIL operation ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 5 ;
                     GET 3 ;
                     DIG 5 ;
                     CAR ;
                     GET ;
                     IF_NONE { PUSH nat 0 } {} ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     PAIR } }
               { IF_LEFT
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     NIL operation ;
                     { DIP 2 { DUP } ; DIG 3 } ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 5 ;
                     CAR ;
                     DIG 5 ;
                     CAR ;
                     GET ;
                     IF_NONE { PUSH nat 0 } {} ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     PAIR }
                   { SWAP ;
                     DUP ;
                     DUG 2 ;
                     NIL operation ;
                     DIG 2 ;
                     CDR ;
                     PUSH mutez 0 ;
                     DIG 4 ;
                     GET 7 ;
                     TRANSFER_TOKENS ;
                     CONS ;
                     PAIR } } }
           { IF_LEFT
               { DUP ;
                 CAR ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 CAR ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 CDR ;
                 GET ;
                 IF_NONE { PUSH nat 0 } {} ;
                 ADD ;
                 ISNAT ;
                 IF_NONE
                   { PUSH string "Cannot burn more than the target's balance." ; FAILWITH }
                   {} ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 CAR ;
                 { DIP 3 { DUP } ; DIG 4 } ;
                 GET 7 ;
                 ADD ;
                 ABS ;
                 DIG 3 ;
                 DUP ;
                 CAR ;
                 PUSH nat 0 ;
                 { DIP 4 { DUP } ; DIG 5 } ;
                 COMPARE ;
                 EQ ;
                 IF { DIG 3 ; DROP ; NONE nat } { DIG 3 ; SOME } ;
                 DIG 4 ;
                 CDR ;
                 UPDATE ;
                 UPDATE 1 ;
                 SWAP ;
                 UPDATE 7 ;
                 NIL operation ;
                 PAIR }
               { SWAP ;
                 DUP ;
                 DUG 2 ;
                 GET 3 ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 CAR ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 CAR ;
                 SENDER ;
                 COMPARE ;
                 EQ ;
                 IF { SWAP }
                    { { DIP 2 { DUP } ; DIG 3 } ;
                      CAR ;
                      SENDER ;
                      SWAP ;
                      PAIR ;
                      { DIP 3 { DUP } ; DIG 4 } ;
                      GET 4 ;
                      { DIP 3 { DUP } ; DIG 4 } ;
                      { DIP 2 { DUP } ; DIG 3 } ;
                      GET ;
                      IF_NONE { PUSH nat 0 } {} ;
                      SUB ;
                      ISNAT ;
                      IF_NONE { PUSH string "NotEnoughAllowance" ; FAILWITH } {} ;
                      DIG 3 ;
                      PUSH nat 0 ;
                      { DIP 2 { DUP } ; DIG 3 } ;
                      COMPARE ;
                      EQ ;
                      IF { SWAP ; DROP ; NONE nat } { SWAP ; SOME } ;
                      DIG 2 ;
                      UPDATE } ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 GET 4 ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 { DIP 4 { DUP } ; DIG 5 } ;
                 CAR ;
                 GET ;
                 IF_NONE { PUSH nat 0 } {} ;
                 SUB ;
                 ISNAT ;
                 IF_NONE { PUSH string "NotEnoughBalance" ; FAILWITH } {} ;
                 DIG 2 ;
                 PUSH nat 0 ;
                 { DIP 2 { DUP } ; DIG 3 } ;
                 COMPARE ;
                 EQ ;
                 IF { SWAP ; DROP ; NONE nat } { SWAP ; SOME } ;
                 { DIP 3 { DUP } ; DIG 4 } ;
                 CAR ;
                 UPDATE ;
                 PUSH int 100 ;
                 PUSH int 4 ;
                 { DIP 4 { DUP } ; DIG 5 } ;
                 GET 4 ;
                 MUL ;
                 EDIV ;
                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                 CAR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 PUSH address "tz1RyejUffjfnHzWoRp1vYyZwGnfPuHsD5F5" ;
                 GET ;
                 IF_NONE { PUSH nat 0 } {} ;
                 ADD ;
                 ISNAT ;
                 { DIP 3 { DUP } ; DIG 4 } ;
                 GET 3 ;
                 UPDATE ;
                 PUSH int 100 ;
                 PUSH int 96 ;
                 { DIP 4 { DUP } ; DIG 5 } ;
                 GET 4 ;
                 MUL ;
                 EDIV ;
                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                 CAR ;
                 SWAP ;
                 DUP ;
                 DUG 2 ;
                 { DIP 4 { DUP } ; DIG 5 } ;
                 GET 3 ;
                 GET ;
                 IF_NONE { PUSH nat 0 } {} ;
                 ADD ;
                 ISNAT ;
                 DIG 4 ;
                 DUG 2 ;
                 DIG 4 ;
                 GET 3 ;
                 UPDATE ;
                 UPDATE 1 ;
                 SWAP ;
                 UPDATE 3 ;
                 NIL operation ;
                 PAIR } } } }
