{ parameter
    (or (or (or (or (or (address %blacklist) (pair %buy_tickets address (pair nat string)))
                    (or (pair %buy_tickets_with_kolibri address (pair nat string))
                        (pair %create_ticket address string)))
                (or (or (pair %kolibri_ticket_price_oracle string (pair timestamp nat))
                        (pair %redeem_tickets address (pair string nat)))
                    (or (address %remove_pool) (unit %reset_kolibri_tx_status))))
            (or (or (or (address %update_admin) (address %update_kolibri_address))
                    (or (address %update_kolibri_recipient) (nat %update_kolibri_ticket_price)))
                (or (or (pair %update_pool_addresses address string)
                        (address %update_storage_contract))
                    (or (mutez %update_ticket_price) (string %update_ticket_types)))))
        (unit %withdraw_balance)) ;
  storage
    (pair (set %admins address)
          (pair (big_map %pool_addresses address string)
                (pair (big_map %blacklist address timestamp)
                      (pair (set %ticket_types string)
                            (pair (mutez %ticket_price)
                                  (pair (int %ticket_validity)
                                        (pair (address %oracle)
                                              (pair (address %kolibri)
                                                    (pair (nat %kolibri_ticket_price)
                                                          (pair (pair %tx_to_kolibri bool (option (pair address (pair address (pair nat string)))))
                                                                (pair (address %kolibri_recipient) (address %storage_contract)))))))))))) ;
  code { UNPAIR ;
         IF_LEFT
           { IF_LEFT
               { IF_LEFT
                   { IF_LEFT
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                                { SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  GET 5 ;
                                  SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  GET ;
                                  IF_NONE
                                    { SWAP ;
                                      DUP ;
                                      GET 5 ;
                                      NOW ;
                                      DIG 3 ;
                                      SWAP ;
                                      SOME ;
                                      SWAP ;
                                      UPDATE ;
                                      UPDATE 5 }
                                    { DROP ;
                                      SWAP ;
                                      DUP ;
                                      GET 5 ;
                                      DIG 2 ;
                                      NONE timestamp ;
                                      SWAP ;
                                      UPDATE ;
                                      UPDATE 5 } } ;
                             NIL operation ;
                             PAIR }
                           { DUP ;
                             CDR ;
                             UNPAIR ;
                             DUP 4 ;
                             GET 5 ;
                             SENDER ;
                             MEM ;
                             IF { DROP 4 ; PUSH string "BLACKLISTED_ADDRESS" ; FAILWITH }
                                { DUP 4 ;
                                  GET 9 ;
                                  SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  MUL ;
                                  AMOUNT ;
                                  COMPARE ;
                                  NEQ ;
                                  PUSH mutez 0 ;
                                  AMOUNT ;
                                  COMPARE ;
                                  NEQ ;
                                  AND ;
                                  IF { DROP 4 ; PUSH string "INCORRECT_AMOUNT" ; FAILWITH }
                                     { SELF_ADDRESS ;
                                       SENDER ;
                                       COMPARE ;
                                       NEQ ;
                                       PUSH mutez 0 ;
                                       AMOUNT ;
                                       COMPARE ;
                                       EQ ;
                                       AND ;
                                       IF { DROP 4 ; PUSH string "INVALID_SENDER" ; FAILWITH }
                                          { DUP 4 ;
                                            GET 19 ;
                                            CAR ;
                                            NOT ;
                                            SELF_ADDRESS ;
                                            SENDER ;
                                            COMPARE ;
                                            EQ ;
                                            PUSH mutez 0 ;
                                            AMOUNT ;
                                            COMPARE ;
                                            EQ ;
                                            AND ;
                                            AND ;
                                            IF { DROP 4 ; PUSH string "INVALID_KOLIBRI_BUY" ; FAILWITH }
                                               { PUSH nat 0 ;
                                                 SWAP ;
                                                 COMPARE ;
                                                 EQ ;
                                                 IF { DROP 3 ; PUSH string "ZERO_TICKET_AMOUNT" ; FAILWITH }
                                                    { DUP 3 ;
                                                      GET 7 ;
                                                      SWAP ;
                                                      MEM ;
                                                      NOT ;
                                                      IF { DROP 2 ; PUSH string "INVALID_TICKET_TYPE" ; FAILWITH }
                                                         { UNPAIR ;
                                                           SWAP ;
                                                           UNPAIR ;
                                                           DUP 4 ;
                                                           GET 22 ;
                                                           CONTRACT %add_tickets
                                                             (pair (address %user) (pair (string %ticket_type) (nat %ticket_amount))) ;
                                                           IF_NONE { PUSH string "UNKNOWN_STORAGE_CONTRACT" ; FAILWITH } {} ;
                                                           PUSH mutez 0 ;
                                                           DIG 4 ;
                                                           DIG 4 ;
                                                           DIG 4 ;
                                                           SWAP ;
                                                           PAIR ;
                                                           SWAP ;
                                                           PAIR ;
                                                           TRANSFER_TOKENS ;
                                                           SWAP ;
                                                           DUP ;
                                                           DUG 2 ;
                                                           GET 19 ;
                                                           CAR ;
                                                           SELF_ADDRESS ;
                                                           SENDER ;
                                                           COMPARE ;
                                                           EQ ;
                                                           PUSH mutez 0 ;
                                                           AMOUNT ;
                                                           COMPARE ;
                                                           EQ ;
                                                           AND ;
                                                           AND ;
                                                           IF { SWAP ;
                                                                NONE (pair address (pair address (pair nat string))) ;
                                                                PUSH bool False ;
                                                                PAIR ;
                                                                UPDATE 19 ;
                                                                NIL operation ;
                                                                DIG 2 ;
                                                                CONS ;
                                                                PAIR }
                                                              { SWAP ; NIL operation ; DIG 2 ; CONS ; PAIR } } } } } } } } }
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 19 ;
                             UNPAIR ;
                             NOT ;
                             IF { DROP ;
                                  DUP ;
                                  CAR ;
                                  SENDER ;
                                  SWAP ;
                                  COMPARE ;
                                  NEQ ;
                                  IF { DROP 2 ; PUSH string "PLAYER_IS_NOT_SENDER" ; FAILWITH }
                                     { SELF_ADDRESS ;
                                       CONTRACT %kolibri_ticket_price_oracle (pair string (pair timestamp nat)) ;
                                       IF_NONE { PUSH string "NO_CONTRACT" ; FAILWITH } {} ;
                                       DUP 3 ;
                                       GET 13 ;
                                       CONTRACT %get (pair string (contract (pair string (pair timestamp nat)))) ;
                                       IF_NONE { PUSH string "NO_ORACLE" ; FAILWITH } {} ;
                                       PUSH mutez 0 ;
                                       DIG 2 ;
                                       PUSH string "XTZ-USD" ;
                                       PAIR ;
                                       TRANSFER_TOKENS ;
                                       DUG 2 ;
                                       SENDER ;
                                       PAIR ;
                                       SOME ;
                                       PUSH bool True ;
                                       PAIR ;
                                       UPDATE 19 ;
                                       NIL operation ;
                                       DIG 2 ;
                                       CONS ;
                                       PAIR } }
                                { SWAP ;
                                  DROP ;
                                  PUSH nat 0 ;
                                  DUP 3 ;
                                  GET 17 ;
                                  COMPARE ;
                                  EQ ;
                                  IF { DROP 2 ; PUSH string "UNKNOWN_KUSD_PRICE" ; FAILWITH }
                                     { IF_NONE { PUSH string "NO_TRANSFER_DETAILS" ; FAILWITH } {} ;
                                       UNPAIR ;
                                       SWAP ;
                                       DUP ;
                                       DUG 2 ;
                                       CDR ;
                                       CAR ;
                                       DUP 4 ;
                                       GET 15 ;
                                       CONTRACT %transfer (pair (address %from) (pair (address %to) (nat %value))) ;
                                       IF_NONE { PUSH string "UNKNOWN_KOLIBRI_TRANSFER" ; FAILWITH } {} ;
                                       DIG 2 ;
                                       DUP 5 ;
                                       GET 21 ;
                                       DUP 6 ;
                                       GET 17 ;
                                       DIG 4 ;
                                       MUL ;
                                       SWAP ;
                                       PAIR ;
                                       SWAP ;
                                       PAIR ;
                                       SWAP ;
                                       PUSH mutez 0 ;
                                       DIG 2 ;
                                       TRANSFER_TOKENS ;
                                       SELF %buy_tickets ;
                                       PUSH mutez 0 ;
                                       DIG 3 ;
                                       TRANSFER_TOKENS ;
                                       DIG 2 ;
                                       NIL operation ;
                                       DIG 2 ;
                                       CONS ;
                                       DIG 2 ;
                                       CONS ;
                                       PAIR } } }
                           { UNPAIR ;
                             DUP 3 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             DUP 4 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             OR ;
                             IF { DUP 3 ;
                                  GET 22 ;
                                  CONTRACT %add_tickets
                                    (pair (address %user) (pair (string %ticket_type) (nat %ticket_amount))) ;
                                  IF_NONE { PUSH string "UNKNOWN_STORAGE_CONTRACT" ; FAILWITH } {} ;
                                  PUSH mutez 0 ;
                                  DIG 2 ;
                                  DIG 3 ;
                                  PUSH nat 1 ;
                                  SWAP ;
                                  PAIR ;
                                  SWAP ;
                                  PAIR ;
                                  TRANSFER_TOKENS ;
                                  SWAP ;
                                  NIL operation ;
                                  DIG 2 ;
                                  CONS ;
                                  PAIR }
                                { DROP 3 ; PUSH string "FORBIDDEN_OP" ; FAILWITH } } } }
                   { IF_LEFT
                       { IF_LEFT
                           { UNPAIR ;
                             SWAP ;
                             UNPAIR ;
                             DUP 4 ;
                             GET 13 ;
                             SENDER ;
                             COMPARE ;
                             NEQ ;
                             IF { DROP 4 ; PUSH string "NOT_FROM_ORACLE" ; FAILWITH }
                                { PUSH string "XTZ-USD" ;
                                  DIG 3 ;
                                  COMPARE ;
                                  NEQ ;
                                  IF { DROP 3 ; PUSH string "WRONG_CURRENCY_PAIR" ; FAILWITH }
                                     { PUSH int 900 ;
                                       NOW ;
                                       SUB ;
                                       SWAP ;
                                       COMPARE ;
                                       LT ;
                                       IF { DROP 2 ; PUSH string "OLD_EXCHANGE_RATE" ; FAILWITH }
                                          { SWAP ;
                                            DUP ;
                                            DUG 2 ;
                                            GET 19 ;
                                            CAR ;
                                            NOT ;
                                            IF { DROP 2 ; PUSH string "NO_ONGOING_TX" ; FAILWITH }
                                               { SWAP ;
                                                 DUP ;
                                                 DUG 2 ;
                                                 GET 19 ;
                                                 CDR ;
                                                 IF_NONE
                                                   { PUSH string "UNKNOWN_BUY_PARAMS" ; FAILWITH }
                                                   { SELF %buy_tickets_with_kolibri ;
                                                     NIL operation ;
                                                     SWAP ;
                                                     PUSH mutez 0 ;
                                                     DIG 3 ;
                                                     CDR ;
                                                     TRANSFER_TOKENS ;
                                                     CONS } ;
                                                 DUP 3 ;
                                                 PUSH nat 1000000 ;
                                                 PUSH mutez 1 ;
                                                 DIG 5 ;
                                                 GET 9 ;
                                                 EDIV ;
                                                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                                                 CAR ;
                                                 DIG 4 ;
                                                 MUL ;
                                                 MUL ;
                                                 UPDATE 17 ;
                                                 SWAP ;
                                                 PAIR } } } } }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             GET 3 ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "UNKNOWN_POOL" ; FAILWITH }
                                { UNPAIR ;
                                  SWAP ;
                                  UNPAIR ;
                                  DUP 4 ;
                                  GET 5 ;
                                  DUP 4 ;
                                  MEM ;
                                  IF { DROP 4 ; PUSH string "BLACKLISTED_PLAYER" ; FAILWITH }
                                     { DUP 4 ;
                                       GET 22 ;
                                       CONTRACT %sub_tickets
                                         (pair (address %user)
                                               (pair (string %ticket_type) (pair (nat %amount_to_sub) (int %ticket_expiry)))) ;
                                       IF_NONE { PUSH string "UNKNOWN_STORAGE_CONTRACT" ; FAILWITH } {} ;
                                       PUSH mutez 0 ;
                                       DIG 4 ;
                                       DIG 3 ;
                                       DIG 4 ;
                                       DUP 6 ;
                                       GET 11 ;
                                       SWAP ;
                                       PAIR ;
                                       SWAP ;
                                       PAIR ;
                                       SWAP ;
                                       PAIR ;
                                       TRANSFER_TOKENS ;
                                       SWAP ;
                                       NIL operation ;
                                       DIG 2 ;
                                       CONS ;
                                       PAIR } } } }
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                                { SWAP ; DUP ; GET 3 ; DIG 2 ; NONE string ; SWAP ; UPDATE ; UPDATE 3 } ;
                             NIL operation ;
                             PAIR }
                           { DROP ;
                             DUP ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP ; PUSH string "NOT_AND_ADMIN" ; FAILWITH }
                                { NONE (pair address (pair address (pair nat string))) ;
                                  PUSH bool False ;
                                  PAIR ;
                                  UPDATE 19 } ;
                             NIL operation ;
                             PAIR } } } }
               { IF_LEFT
                   { IF_LEFT
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                                { SWAP ;
                                  DUP ;
                                  CAR ;
                                  PUSH bool False ;
                                  SENDER ;
                                  UPDATE ;
                                  DIG 2 ;
                                  PUSH bool True ;
                                  SWAP ;
                                  UPDATE ;
                                  UPDATE 1 } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH } { UPDATE 15 } ;
                             NIL operation ;
                             PAIR } }
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH } { UPDATE 21 } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AND_ADMIN" ; FAILWITH } { UPDATE 17 } ;
                             NIL operation ;
                             PAIR } } }
                   { IF_LEFT
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                                { UNPAIR ;
                                  DIG 2 ;
                                  DUP ;
                                  DUP ;
                                  DUG 4 ;
                                  GET 3 ;
                                  DUP 3 ;
                                  GET ;
                                  IF_NONE
                                    { DIG 3 ; GET 3 ; DIG 3 ; DIG 3 ; SWAP ; SOME ; SWAP ; UPDATE }
                                    { DROP ; DIG 3 ; GET 3 ; DIG 3 ; SOME ; DIG 3 ; UPDATE } ;
                                  UPDATE 3 } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UPDATE 22 } ;
                             NIL operation ;
                             PAIR } }
                       { IF_LEFT
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UPDATE 9 } ;
                             NIL operation ;
                             PAIR }
                           { SWAP ;
                             DUP ;
                             DUG 2 ;
                             CAR ;
                             SENDER ;
                             MEM ;
                             NOT ;
                             IF { DROP 2 ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                                { SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  GET 7 ;
                                  SWAP ;
                                  DUP ;
                                  DUG 2 ;
                                  MEM ;
                                  IF { SWAP ; DUP ; GET 7 ; DIG 2 ; PUSH bool False ; SWAP ; UPDATE ; UPDATE 7 }
                                     { SWAP ; DUP ; GET 7 ; DIG 2 ; PUSH bool True ; SWAP ; UPDATE ; UPDATE 7 } } ;
                             NIL operation ;
                             PAIR } } } } }
           { DROP ;
             DUP ;
             CAR ;
             SENDER ;
             MEM ;
             NOT ;
             IF { DROP ; PUSH string "NOT_AN_ADMIN" ; FAILWITH }
                { NIL operation ;
                  SWAP ;
                  DUP ;
                  DUG 2 ;
                  CAR ;
                  ITER { CONTRACT unit ;
                         IF_NONE
                           {}
                           { PUSH mutez 1 ;
                             PUSH mutez 1 ;
                             DUP 5 ;
                             CAR ;
                             SIZE ;
                             MUL ;
                             BALANCE ;
                             EDIV ;
                             IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                             CAR ;
                             MUL ;
                             UNIT ;
                             TRANSFER_TOKENS ;
                             CONS } } ;
                  PAIR } } } }
